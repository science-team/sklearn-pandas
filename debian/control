Source: sklearn-pandas
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Federico Ceratto <federico@debian.org>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
Build-Depends-Indep:
 dh-sequence-python3,
 python3-all,
 python3-numpy,
 python3-pandas (>= 2.0.0),
 python3-pytest,
 python3-setuptools,
 python3-sklearn,
Standards-Version: 4.6.2
Homepage: https://github.com/scikit-learn-contrib/sklearn-pandas
Vcs-Git: https://salsa.debian.org/science-team/sklearn-pandas.git
Vcs-Browser: https://salsa.debian.org/science-team/sklearn-pandas
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no
X-Style: black

Package: python3-sklearn-pandas
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Enhances:
 python3-pandas,
 python3-sklearn,
Description: Pandas integration with sklearn (Python 3)
 sklearn-pandas provides a bridge between scikit-learn's machine learning
 methods and pandas data frames.
 .
 In particular, it provides:
  - a way to map DataFrame columns to transformations, which are later
    recombined into features
  - a way to cross-validate a pipeline that takes a pandas DataFrame as input.
 .
 This is the Python 3 version of the package.
